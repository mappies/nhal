﻿using Newtonsoft.Json;

namespace NHal.Tests
{
    public class LinkOne : HalObject
    {
        [JsonProperty("pi")]
        public double Pi { get; set; }
    }
}

