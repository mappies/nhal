﻿using Newtonsoft.Json;

namespace NHal.Tests
{
    public class Item : HalObject
    {
        [JsonProperty("item")]
        public string Name { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }
    }
}

