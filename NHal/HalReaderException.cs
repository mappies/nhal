﻿using System;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace NHal
{
    public class HalReaderException : Exception
    {
        public HalReaderException(string message) : base(message) { }
        public HalReaderException(string message, Exception e) : base(message, e) { }

        public T Get<T>() where T : HalObject
        {
            if (this.InnerException == null || !(this.InnerException is FlurlHttpException))
            {
                return default(T);
            }

            var exception = (FlurlHttpException)this.InnerException;

            var response = exception.GetResponseString();

            try
            {
                return JObject.Parse(response).ToObject<T>();
            }
            catch(JsonReaderException e)
            {
                var message = string.Format("The error payload is not json.  Found '{0}{1}'", 
                                            response.Substring(0, response.Length > 20 ? 20 : response.Length),
                                            (response.Length < 20) ? string.Empty : "...");
                throw new HalReaderException(message, e);
            }
        }
    }
}

