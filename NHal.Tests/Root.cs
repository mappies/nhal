﻿using Newtonsoft.Json;

namespace NHal.Tests
{
    public class Root : HalObject
    {
        [JsonProperty("number")]
        public int Number { get; set; }

        [JsonProperty("string")]
        public string String { get; set; }

        [JsonProperty("bool")]
        public bool Bool { get; set; }
    }
}

