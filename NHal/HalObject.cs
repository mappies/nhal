﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NHal
{
    public class HalObject
    {
        private IDictionary<string, Link> links;

        [JsonProperty("_links")]
        public IDictionary<string, Link> Links
        {
            get
            {
                if (this.links == null)
                {
                    this.links = new Dictionary<string, Link>();
                }
                return this.links;
            }
        }
    }
}

