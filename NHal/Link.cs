﻿using Newtonsoft.Json;

namespace NHal
{
    public class Link
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }
}

