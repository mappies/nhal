﻿using Newtonsoft.Json;

namespace NHal.Tests
{
    public class Error : HalObject
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}

