﻿using NUnit.Framework;
using System;
using System.IO;
using System.Threading.Tasks;
using Flurl.Http.Testing;

namespace NHal.Tests
{
    [TestFixture]
    public class Test
    {
        private string FromAsset(string name)
        {
            return File.ReadAllText(string.Format("{0}.json", Path.Combine("Assets", name)));
        }

        private async Task<HalReader> GetReader()
        {
            return await HalReader.CreateAsync(new Uri("http://www.example.com"));
        }

        [Test]
        public async void TestGetLinks()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                var links = (await this.GetReader()).GetLinks();

                Assert.AreEqual(3, links.Count);
                Assert.AreEqual("https://example.com/", links["self"].Href);
                Assert.AreEqual("https://example.com/one", links["linkone"].Href);
                Assert.AreEqual("https://example.com/two", links["linktwo"].Href);
            }
        }

        [Test]
        public async void TestGetLink()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                Assert.AreEqual("https://example.com/two", ((await this.GetReader()).GetLink("linktwo")).Href);
            }
        }

        [Test]
        [ExpectedException(typeof(HalReaderException), ExpectedMessage="The return payload is not json.  Found 'This is not a json f...'")]
        public async void TestNotJsonPayload()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("notjson"));

                await this.GetReader();
            }
        }

        [Test]
        public async void TestBadResponse()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(500, "server error");

                try
                {
                    await this.GetReader();

                    Assert.Fail("HalReaderException expected.");
                }
                catch(HalReaderException e1)
                {
                    try
                    {
                        e1.Get<Error>();

                        Assert.Fail("HalReaderException expected.");
                    }
                    catch(HalReaderException)
                    {
                        Assert.Pass();
                    }
                }
            }
        }

        [Test]
        public async void TestBadResponseWithJson()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWithJson(400, new {code=400, message="Bad Data"});

                try
                {
                    await this.GetReader();
                    Assert.Fail("Expected an exception.");
                }
                catch(HalReaderException e)
                {
                    var error = e.Get<Error>();

                    Assert.AreEqual(400, error.Code);
                    Assert.AreEqual("Bad Data", error.Message);
                }
                catch(Exception)
                {
                    Assert.Fail("Unexpected an exception.");
                }
            }
        }

        [Test]
        public async void TestGetInvalidLink()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                Assert.IsNull((await this.GetReader()).GetLink("invalid"));
            }
        }

        [Test]
        public async void TestFollowsLink()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));

                var client = await this.GetReader();

                await client.FollowAsync("linkone");

                Assert.AreEqual("https://example.com/one?start=0&limit=10", client.GetLink("self").Href);
                Assert.IsNull(client.GetLink("prev"));
                Assert.AreEqual("https://example.com/one?start=10&limit=10", client.GetLink("next").Href);
            }
        }

        [Test]
        public async void TestFollowsLinkWithUri()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));

                var client = await this.GetReader();

                await client.FollowAsync(new Uri("http://example.com"));

                Assert.AreEqual("https://example.com/one?start=0&limit=10", client.GetLink("self").Href);
                Assert.IsNull(client.GetLink("prev"));
                Assert.AreEqual("https://example.com/one?start=10&limit=10", client.GetLink("next").Href);
            }
        }

        [Test]
        public async void TestFollowsLinkWithObject()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));
                httpTest.RespondWith(this.FromAsset("linkone_a"));

                var client = await this.GetReader();

                await client.FollowAsync("linkone");

                var items = client.GetMany<Item>("items");

                await client.FollowAsync(items[0]);

                var item = client.Get<Item>();

                Assert.AreEqual("DataA", item.Data);
            }
        }

        [Test]
        public async void TestBack()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));
                httpTest.RespondWith(this.FromAsset("linkone_a"));

                var client = await this.GetReader();

                await client.FollowAsync("linkone");

                var items = client.GetMany<Item>("items");

                await client.FollowAsync(items[0]);

                var item = client.Get<Item>();

                Assert.AreEqual("DataA", item.Data);

                client.Back();
                var linkOne = client.Get<LinkOne>();
                Assert.AreEqual(3.141592654, linkOne.Pi);

                client.Back();
                var root = client.Get<Root>();
                Assert.AreEqual("hi", root.String);
            }
        }

        [Test]
        [ExpectedException(typeof(HalReaderException), ExpectedMessage="Could not follow rel 'invalid'. The rel could not be found.")]
        public async void TestFollowsInvalidLink()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                var client = await this.GetReader();

                await client.FollowAsync("invalid");

            }
        }

        [Test]
        public async void TestFollowsLinksBackAndForth()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linktwo"));

                var client = await this.GetReader();

                await client.FollowAsync("linkone");
                await client.FollowAsync("up");
                await client.FollowAsync("linktwo");

                Assert.AreEqual("https://example.com/two?limit=10&start=0", client.GetLink("self").Href);
            }
        }

        [Test]
        public async void TestGetHalObject()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                var client = await this.GetReader();

                var root = client.Get<HalObject>();

                Assert.AreEqual(3, root.Links.Count);
                Assert.AreEqual("https://example.com/", root.Links["self"].Href);
                Assert.AreEqual("https://example.com/one", root.Links["linkone"].Href);
                Assert.AreEqual("https://example.com/two", root.Links["linktwo"].Href);
            }
        }

        [Test]
        public async void TestGetObject()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));

                var client = await this.GetReader();

                var root = client.Get<Root>();

                Assert.AreEqual(123, root.Number);
                Assert.AreEqual("hi", root.String);
                Assert.IsFalse(root.Bool);

                Assert.AreEqual(3, root.Links.Count);
            }
        }

        [Test]
        public async void TestFollowAndGetObject()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("root"));
                httpTest.RespondWith(this.FromAsset("linkone"));

                var client = await this.GetReader();

                await client.FollowAsync("linkone");

                var linkOne = client.Get<LinkOne>();

                Assert.AreEqual(3.141592654, linkOne.Pi);

                Assert.AreEqual(4, linkOne.Links.Count);
                Assert.AreEqual("https://example.com/one?start=0&limit=10", linkOne.Links["self"].Href);
            }
        }

        [Test]
        public async void TestGetMany()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("list"));

                var client = await this.GetReader();

                var items = client.GetMany<Item>();

                Assert.AreEqual(2, items.Count);
                Assert.AreEqual("A", items[0].Name);
                Assert.AreEqual("B", items[1].Name);

                Assert.AreEqual(1, items[0].Links.Count);
                Assert.AreEqual(1, items[1].Links.Count);

                Assert.AreEqual("https://example.com/list/A", items[0].Links["self"].Href);
                Assert.AreEqual("https://example.com/list/B", items[1].Links["self"].Href);
            }
        }

        [Test]
        public async void TestGetManyFromProperty()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("linkone"));

                var client = await this.GetReader();

                var items = client.GetMany<Item>("items");

                Assert.AreEqual(2, items.Count);
                Assert.AreEqual("A", items[0].Name);
                Assert.AreEqual("B", items[1].Name);

                Assert.AreEqual(1, items[0].Links.Count);
                Assert.AreEqual(1, items[1].Links.Count);

                Assert.AreEqual("https://example.com/one/A", items[0].Links["self"].Href);
                Assert.AreEqual("https://example.com/one/B", items[1].Links["self"].Href);
            }
        }

        [Test]
        [ExpectedException(typeof(HalReaderException), ExpectedMessage="Property 'invalid' could not be found.")]
        public async void TestGetManyInvalidRel()
        {
            using (var httpTest = new HttpTest())
            {
                httpTest.RespondWith(this.FromAsset("list"));

                var client = await this.GetReader();

                client.GetMany<Item>("invalid");
            }
        }
    }
}

