﻿using System;
using System.IO;
using System.Threading.Tasks;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NHal
{
    public class HalReader : IDisposable
    {
        private FlurlClient Client { get; set; }

        private JToken Response 
        { 
            get { return this.Responses.Peek(); }
        }

        private Stack<JToken> responses;
        private Stack<JToken> Responses 
        { 
            get
            {
                return responses ?? (responses = new Stack<JToken>());
            }
        }

        /// <summary>
        /// Create an Instance of HalReader.
        /// </summary>
        /// <param name="uri">The intial URI</param>
        public static async Task<HalReader> CreateAsync(Uri uri)
        {
            var reader = new HalReader(uri);

            await reader.GetResponseAsync();

            return reader;
        }

        /// <summary>
        /// Create an instance of HalReader.
        /// </summary>
        /// <param name="url">The intial URI</param>
        private HalReader(Uri url)
        {
            this.Client = new FlurlClient(url.ToString());
        }

        /// <summary>
        /// Fetchs the given path.
        /// </summary>
        /// <returns>The response async.</returns>
        /// <param name="path">Relative or absolute uri to fetch</param>
        private async Task GetResponseAsync(string path = null)
        {
            var uri = path == null ? this.Client.Url.ToString() : new Uri(new Uri(this.Client.Url), path).ToString();
            string response = string.Empty;

            try
            {
                response = await uri.GetStringAsync();
            }
            catch(FlurlHttpException e)
            {
                throw new HalReaderException(e.Message, e);
            }

            try
            {
                this.Responses.Push(JToken.Parse(response));
            }
            catch(JsonReaderException e)
            {
                var message = string.Format("The return payload is not json.  Found '{0}{1}'", 
                                response.Substring(0, response.Length > 20 ? 20 : response.Length),
                                (response.Length < 0) ? string.Empty : "...");
                throw new HalReaderException(message, e);
            }
        }

        /// <summary>
        /// Releases all resource used by the object.
        /// </summary>
        public void Dispose()
        {
            this.Client.Dispose ();
        }

        /// <summary>
        /// Gets a link by the given rel from the current page.
        /// </summary>
        /// <returns>The link or null if the rel could not be found.</returns>
        /// <param name="rel">Rel.</param>
        public Link GetLink(string rel)
        {
            var links = this.GetLinks();

            return links.ContainsKey(rel) ? links[rel] : null;
        }

        /// <summary>
        /// Gets all links of the current page.
        /// </summary>
        /// <returns>All links.</returns>
        public IDictionary<string, Link> GetLinks()
        {
            var obj = this.Get<HalObject>();

            return obj.Links;
        }

        /// <summary>
        /// Back to the previous page.
        /// </summary>
        public async void Back()
        {
            this.Responses.Pop();
        }

        /// <summary>
        /// Follows the specified URI.
        /// </summary>
        /// <param name="uri">Uri</param>
        public async Task FollowAsync(Uri uri)
        {
            await this.GetResponseAsync(uri.ToString());
        }

        /// <summary>
        /// Follows the specified rel to the next page.
        /// </summary>
        /// <param name="rel">Rel.</param>
        public async Task FollowAsync(string rel)
        {
            var link = this.GetLink(rel);

            if(link == null)
            {
                throw new HalReaderException(string.Format("Could not follow rel '{0}'. The rel could not be found.", rel));
            }

            await this.GetResponseAsync(link.Href);
        }

        /// <summary>
        /// Follows the specified rel to the next page.
        /// </summary>
        /// <param name="obj">The HalObject from a collection</param>
        /// <param name="rel">Rel.</param>
        public async Task FollowAsync(HalObject obj, string rel = "self")
        {
            if(!obj.Links.ContainsKey(rel))
            {
                throw new HalReaderException(string.Format("Could not follow rel '{0}'. The rel could not be found.", rel));
            }

            var link = obj.Links[rel];

            await this.GetResponseAsync(link.Href);
        }

        /// <summary>
        /// Gets the object from the current page.
        /// </summary>
        public T Get<T>() where T : HalObject
        {
            return this.Response.ToObject<T>();
        }

        /// <summary>
        /// Gets a list of objects from the current page.
        /// </summary>
        /// <param name="property">A property to get a list of objects. 
        /// Leaves empty to get a list of objects from the current page.</param>
        public IList<T> GetMany<T>(string property = null) where T : HalObject
        {
            var token = this.Response;

            if (!string.IsNullOrEmpty(property))
            {
                token = token.SelectToken(property);
            }

            if (token == null)
            {
                var message = string.Format("Property '{0}' could not be found.", property);
                throw new HalReaderException(message);
            }

            var result = new List<T>();

            foreach (var item in token.Children())
            {
                result.Add(item.ToObject<T>());
            }

            return result;
        }
    }
}

